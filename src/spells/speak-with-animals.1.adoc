include::dict/dict.1.adoc[]

= Speak with Animals

:school: {alteration}
:casting-time: 5 segments
:range: 0
:area: One Animal within {3aau} radius of the cleric
:components: {v-s}
:duration: {2-rounds} / level
include::template/spell-template.adoc[]

By employing this spell, the cleric is empowered to comprehend and communicate with any warm or cold-blooded animal which is not mindless (such as an amoeba). The cleric is able to ask questions, receive answers, and generally be on *amicable terms with the animal*.

This ability lasts for {2-rounds} for each level of experience of
the cleric employing the spell.

Even if the *bent* of the animal *is opposite to that of the cleric* (evil/good, good/evil), *it and any others of the same kind* with it *will not attack* while the spell lasts. If the animal *is neutral* or *of the same general bent* as the cleric (evil/evil, good/good), there is a *possibility that the animal, and its like associates, will do some favor or service for the cleric*.

This possibility will be determined by the referee by consulting a special reaction chart, using the charisma of the cleric and his actions as the major determinants. Note that this spell differs from speak with monsters (q.v.), for it allows conversation only with basically *normal, non-fantastic creatures such as apes, bears, cats, dogs, elephants*, and so on.
