include::dict/dict.1.adoc[]

= Light / Licht

:school: {alteration}
:reversible: true
:casting-time: 4 segments
:range: {12aa}
:area: {2aau} radius globe
:components: {v-s}
:duration: {6-turns} + {1-turn} / level
include::template/spell-template.adoc[]

This spell causes excitation of molecules so as to make them brightly luminous. The light thus caused is *equal to torch light* in brightness, but its sphere is limited to {4aau} in diameter.

It lasts for the duration indicated ({7-turns} at 1st experience level, {8-turns} at 2nd, {9-turns} at 3rd. etc.)  or until the caster utters a word to extinguish the light.

The light spell is _reversible_, causing darkness in the same area and under the same conditions, except the *blackness* persists for only *one-half the duration that light would last*.

If this spell is cast upon a *creature*, the applicable *magic resistance and saving throw dice rolls* must be made. Success indicates that the spell affects the area *immediately behind the creature*, rather than
the creature itself.
In all other cases, the spell takes effect where the caster directs as long as he or she has a line of sight or unobstructed path for the spell; light can spring from air, rock, metal, wood, or almost any similar substance.
