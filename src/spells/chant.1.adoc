include::dict/dict.1.adoc[]

= Chant

:school: {conjuration}
:casting-time: {1-turn}
:range: 0
:area: {3aau} radius
:components: {v-s} (a sprinkling of holy water)
:duration: Time of chanting
include::template/spell-template.adoc[]

By means of the chant, the cleric brings into being a special favor upon himself or herself and his or her party, and causes harm to his or her enemies.

Once the chant spell is completed, all *attacks, damage and saving throws* made by those in the area of effect who are *friendly to the cleric* are at *+1*, while those of the cleric's *enemies* are at *-1*.

This bonus/penalty continues *as long as the cleric continues to chant* the mystic syllables and *is stationary*.

An *interruption*, however, such as an *attack which succeeds and causes damage, grappling the chanter, or a magical silence, will break the spell*.


