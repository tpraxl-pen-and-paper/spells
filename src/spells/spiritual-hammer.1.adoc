include::dict/dict.1.adoc[]

= Spiritual Hammer

:school: {invocation}
:casting-time: 5 segments
:range: {3aa}
:area: One opponent
:components: {v-s-m} (normal war hammer – disappears when spell is cast)
:duration: {1-round} / level
include::template/spell-template.adoc[]

By calling upon his or her deity, the cleric casting a spiritual hammer spell brings into existence a field of force which is shaped vaguely like a hammer. This area of force is hammer-sized, and *as long as the cleric who invoked it concentrates upon the hammer, it will strike at any opponent within its range as desired by the cleric*. The *force
area strikes as a magical weapon* equal to *one plus per 3 levels* of
experience of the spell caster for purposes of being able to strike
creatures, although it has *no magical plusses* whatsoever "to hit", and the damage it causes when it scores a hit is exactly the same as a normal war hammer, i.e. *1-6 versus opponents of man-size or smaller, 1-4 upon larger opponents*. Furthermore, the hammer strikes at exactly the same level as the cleric controlling it, just as if the cleric was personally wielding the weapon. As soon as the cleric ceases concentration, the spiritual hammer is dispelled. Note: If the cleric is behind an opponent, the force can strike
from this position, thus gaining all bonuses for such an attack and negating defensive protections such as shield and dexterity. The material
component of this spell is _a normal war hammer which the cleric must hurl
towards opponents whilst uttering a plea to his or her deity_. The *hammer
disappears* when the spell is cast.
