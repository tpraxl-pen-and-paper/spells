include::dict/dict.1.adoc[]

= Cure Disease

:school: {abjuration}
:reversible: true
:casting-time: {1-turn}
:range: {touch}
:area: Creature touched
:components: {v-s}
:duration: Permanent
include::template/spell-template.adoc[]

The cleric *cures most diseases – including those of a parasitic, bacterial, or viral nature* - by placing his or her hand upon the diseased creature. The affliction *rapidly disappears* thereafter, making the cured creature whole and well in *from {1-turn} to 1 week*, depending on the kind of disease and the state of its advancement when the cure took place.

The *reverse* of the cure disease spell is *cause disease*. To be effective, the cleric must touch the intended victim, and the victim must *fail the saving throw*. The disease caused will *begin to affect the victim in 1-6 turns*, causing the afflicted creature to lose *1 hit point per turn, and 1 point of strength per hour, until the creature is at 10% of original hit points and strength, at which time the afflicted is weak and virtually helpless*.

