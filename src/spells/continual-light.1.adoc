include::dict/dict.1.adoc[]

= Continual Light

:school: {alteration}
:reversible: true
:casting-time: 6 segments
:range: {12aa}
:area: {6aau} radius globe
:components: {v-s}
:duration: Permanent
include::template/spell-template.adoc[]

This spell is similar to a light spell, except that it *lasts until negated* (by a continual darkness or dispel magic spell) and its brightness is very great, being *nearly as illuminating as full  daylight*.

It can be cast into air, onto an object, or at a creature. In the third case, the continual light affects the *space about {1-foot} behind the creature} if the latter makes its saving throw*.
Note that this spell will *blind a creature* if it is successfully cast upon the visual organs, for example. Its *reverse causes complete absence of light*.
