include::dict/dict.1.adoc[]

= Animate Dead

:school: {necromancy}
:casting-time: {1-round}
:range: {1aa}
:area: Special
:components: {v-s-m} (drop of blood, piece of human flesh, pinch of bone powder or a bone shard)
:duration: Permanent
include::template/spell-template.adoc[]

This spell creates the lowest of the undead monsters, skeletons or zombies, from the bones or bodies of *dead humans*. The effect is to cause these remains to become *animated and obey the commands* of the cleric casting the spell.

The skeletons or zombies will follow, remain in an area and attack any creature (or just a specific type of creature) entering the place, etc.

The spell will animate the monsters *until they are destroyed or until the magic is dispelled. (See dispel magic spell).*

The cleric is able to animate *1 skeleton or 1 zombie for each level* of experience he or she has attained. Thus, a 2nd level cleric can animate 2
of these monsters, a 3rd level 3, etc. The act of animating dead is *not basically a good one*, and it must be used with careful consideration and *good reason* by clerics of good alignment. It requires a _drop of blood_, a _piece of human flesh_, and a _pinch of bone powder or a bone shard_ to complete the spell.
