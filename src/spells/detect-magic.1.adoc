include::dict/dict.1.adoc[]

= Detect Magic / Magie entdecken

:school: {divination}
:casting-time: {1-round}
:range: {3aa}
:area: {1aau} path, {3aau} long
:components: {v-s-m}
:duration: {1-turn}
include::template/spell-template.adoc[]

When the detect magic spell is cast, the cleric detects magical radiations in a path {1aau} wide, and up to {3aau} long, in the
direction he or she is facing. The caster can turn 60° per {round}.

Note that stone walls of {1-inch} or more thickness, solid metal of but {1-12th-inch} thickness, or {3-inch} or more of solid wood will block the spell. The spell requires the use of the cleric's holy (or unholy) symbol.
