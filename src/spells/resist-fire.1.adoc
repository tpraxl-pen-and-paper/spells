include::dict/dict.1.adoc[]

= Resist Fire

:school: {alteration}
:casting-time: 5 segments
:range: {touch}
:area: Creature touched
:components: {v-s-m} (drop of mercury)
:duration: {1-turn} / level
include::template/spell-template.adoc[]

When this spell is placed upon a creature by a cleric, the creature's body is toughened to withstand heat, and *boiling temperature is comfortable*.

The recipient of the resist fire spell can even stand in the midst of *very hot or magical fires* such as those produced by red-hot charcoal, a large amount of burning oil, flaming swords, fire storms, fire balls, meteor swarms, or red dragon's breath - but these will *affect the creature, to some extent*. The recipient of the spell *gains a bonus
of +3 on saving throws* against such attack forms, and all *damage*
sustained is *reduced by 50%*; therefore, if the *saving throw is not made*, the creature sustains *one-half damage*, and if the *saving throw is made* only *one-quarter damage* is sustained.

Resistance to fire lasts for {1-turn} for each level of experience of the cleric placing the spell. The caster needs a _drop of mercury_ as the material component of this spell.
