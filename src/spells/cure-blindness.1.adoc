include::dict/dict.1.adoc[]

= Cure Blindness

:school: {abjuration}
:reversible: true
:casting-time: {1-round}
:range: {touch}
:area: Creature touched
:components: {v-s}
:duration: Permanent
include::template/spell-template.adoc[]

By touching the creature afflicted, the cleric employing the spell can permanently cure most forms of blindness. Its *reverse, cause blindness*, requires a successful touch upon the victim, and if the victim then makes the *saving throw*, the effect is negated.
