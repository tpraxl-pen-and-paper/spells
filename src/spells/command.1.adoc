include::dict/dict.1.adoc[]

= Command / Befehl

:school: {enchantment}
:casting-time: 1 segment
:range: {1aa}
:area: 1 Creature
:components: {v}
:duration: {1-round}
include::template/spell-template.adoc[]

This spell enables the cleric to issue a command of a single word. The command must be uttered in a **language** which the spell recipient is **able to understand**. The individual will obey to the best of his/her/its ability only so long as the command is absolutely clear and unequivocal, i.e. "Suicide!" could be a noun, so the creature would ignore the command. A command to "Die!" would cause the recipient to fall in a faint or cataleptic state for {1-round}, but thereafter the creature would be alive and well. Typical command words are: __back, halt, flee, run, stop, fall, fly, go, leave, surrender, sleep, rest,__ etc. (also: __approach, drop__) **Undead are not affected by a command**.

Creatures with **intelligence of 13 or more, and creatures with 6
or more hit dice (or experience levels)** are entitled to a **saving throw
versus magic**. (Creatures with 13 or higher intelligence and 6 hit
dice/levels do not get 2 saving throws!)
