{school}
ifeval::["{reversible}" == "true"]
Reversible
endif::[]

[cols="h,"]
|===
| Casting time | {casting-time}
| Range | {range}
ifeval::["{area}" != ""]
| Area | {area}
endif::[]
| Components | {components}
| Duration | {duration}
|===

// reset parameters
:reversible:
:area:
