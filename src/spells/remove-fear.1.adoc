include::dict/dict.1.adoc[]

= Remove Fear

:school: {abjuration}
:reversible: true
:casting-time: 4 segments
:range: {touch}
:area: Touch
:components: {v-s}
:duration: Special
include::template/spell-template.adoc[]

By touch, the cleric instills courage in the spell recipient, *raising the creature's saving throw against magical fear attacks by + 4 on dice rolls for 1 turn*.

If the recipient has already been affected by fear, and failed the appropriate saving throw, the touch allows *another saving throw to be made, with a bonus of 1 on the dice for every level of
experience of the caster*, i.e. a 2nd level cleric gives a + 2 bonus, a 3rd level +3, etc.

A "to hit" dice roll must be made to touch an unwilling recipient. The reverse of the spell, *cause fear*, causes the victim to flee in panic at maximum movement speed away from the caster for *1 round per level of the cleric causing such fear*. Of course, cause fear can be countered by remove fear and vice versa.
