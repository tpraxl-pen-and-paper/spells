:toc:
:toc-title: Übersicht

= 3rd Level / Grad 3

:leveloffset: +1

include::spells/animate-dead.1.adoc[]

include::spells/continual-light.1.adoc[]

include::spells/create-food-and-water.1.adoc[]

include::spells/cure-blindness.1.adoc[]

include::spells/cure-disease.1.adoc[]

include::spells/dispel-magic.1.adoc[]

include::spells/feign-death.1.adoc[]

include::spells/glyph-of-warding.1.adoc[]

include::spells/locate-object.1.adoc[]

include::spells/prayer.1.adoc[]

include::spells/remove-curse.1.adoc[]

include::spells/speak-with-the-dead.1.adoc[]

:leveloffset: -1
